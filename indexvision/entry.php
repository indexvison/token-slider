
<?php
$get_cat        = get_the_category();
$first_cat      = $get_cat[0];
$category_name  = $first_cat->cat_name;
$category_link  = get_category_link( $first_cat->cat_ID );
?>

<article class="token-card">
    <div class="token-card-content">
        <div class="load-bg"></div>
        <div class="token-card-head">
            <div class="token-icon" style="background-image: url(<?php echo the_field('token_icon') ?>)"></div>
            <div class="token-name"><?php the_title(); ?></div>
            <a href="<?php echo esc_url( $category_link ); ?>#s<?php echo $itemCount; ?>"
               class="token-link-copied"></a>
        </div>
        <div class="token-card-body">
            <div class="token-card-body-title">
                <h1 class="token-card-full-name"><?php echo the_field('token_full_name') ?></h1>
                <div class="token-card-desc"><?php echo the_field('token_desc') ?></div>
            </div>

            <ul class="token-card-copy">
                click to copy
                <?php if (have_rows('token_copy_list')): ?>
                    <?php while (have_rows('token_copy_list')): the_row(); ?>
                        <li>
                            <a href="<?php echo the_sub_field('token_copy_list_item') ?>"><?php echo the_sub_field('token_copy_list_item') ?></a>
                        </li>
                    <?php endwhile; ?>
                <?php endif; ?>

            </ul>
            <div class="token-card-percent bold-number">≈ <?php echo the_field('token_percent') ?> %</div>
        </div>
    </div>
    <div class="token-icon-for-ellipse"
         style="background-image: url(<?php echo the_field('token_icon') ?>)"></div>
</article>