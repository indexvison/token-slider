let deviceAgent = navigator.userAgent.toLowerCase();
const body = jQuery("body");
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
    body.addClass("ios");
    body.addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
    body.addClass("ie");
} else if (navigator.userAgent.search("Chrome") >= 0) {
    body.addClass("chrome");
} else if (navigator.userAgent.search("Firefox") >= 0) {
    body.addClass("firefox");
} else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
    body.addClass("safari");
} else if (navigator.userAgent.search("Opera") >= 0) {
    body.addClass("opera");
}

jQuery(document).on('ready', function () {
    jQuery('body').addClass('load');
});