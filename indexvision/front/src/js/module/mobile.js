// Перемещение элементов при мобильниках
if (window.matchMedia("(max-width: 1200px)").matches && window.matchMedia("(min-width: 767px)").matches) {
    jQuery('meta[name="viewport"]').attr('content', 'width=460px');
}
