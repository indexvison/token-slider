jQuery(document).on('ready', function () {

    if (jQuery('.token-slider').length) {

        const visibleCount = Number(jQuery('.token-slider').attr('data-code-option'));

        jQuery('.token-card:not(.slick-cloned)').each(function () {
            jQuery(this).find('.token-card-codes li').each(function () {
                let code = jQuery(this).find('a').text(),
                    str = '';

                str = code.slice(0, visibleCount) + '...' + code.slice(-visibleCount);

                jQuery(this).find('a').text(str);
            });
        });
    }

});