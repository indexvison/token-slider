import linkParams from './link-params';

jQuery(document).on('ready', function () {

    // slider
    const $slider = jQuery(".token-slider");

    if ($slider.length) {
        $slider.slick({
            dots: true,
            arrows: false,
            vertical: true,
            verticalSwiping:true,
            infinite: true,
            adaptiveHeight: false,
            speed: 800,
            appendDots: jQuery('.token-slider-pagin'),
        })

        jQuery('.token-ellipse svg g:nth-child(2)').addClass('active');

        $slider.on('wheel', (function (e) {
            e.preventDefault();

            if (e.originalEvent.deltaY > 0) {
                jQuery(this).slick('slickNext');
            } else {
                jQuery(this).slick('slickPrev');
            }

        }));

        $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            nextSlide = nextSlide + 1;
            let wheelCount = 60 * nextSlide;

            jQuery('.token-ellipse svg g').removeClass('active');

            jQuery('.token-ellipse svg').css('transform', 'rotate(-' + wheelCount + 'deg)');

            if (nextSlide > 6) {
                nextSlide = nextSlide - 6;
            }

            jQuery('.token-ellipse svg g:nth-child(' + (nextSlide + 1) + ')').addClass('active');

        });

        jQuery('.token-card:not(.slick-cloned)').each(function (index,item ) {
            let thisIndex = index + 1,
                tokenName = jQuery(this).find('.token-name').text();

            jQuery('.token-slider-pagin li').eq(index).find('button').html('<span>'+tokenName+'</span>');

        })
    }

    // slick go to
    jQuery('.token-slider').slick('slickGoTo', linkParams)

})

