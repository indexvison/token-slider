jQuery(document).on('ready', function () {

    // click to copy code
    copyTrigger('token-card-codes a');

    // click to copy card
    copyTrigger('token-card-copied');

    function copyTrigger(trigger) {
        let thisTrigger = jQuery('.' + trigger);

        thisTrigger.on('click', function (e) {
            e.preventDefault();

            let copyText = jQuery(this).attr('href'),
                thisElem = jQuery(this);

            copyToClipboard(copyText, thisElem);
        });
    }

    function copyToClipboard(message, elem) {
        var textArea = document.createElement("textarea");
        textArea.value = message;
        textArea.style.opacity = "0";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';

            console.log(jQuery(elem).parent())
            jQuery(elem).siblings('.copy-message').addClass('is-copy');

            setTimeout(function () {
                jQuery(elem).siblings('.copy-message').removeClass('is-copy');
            }, 1000);
        } catch (err) {
            alert('Unable to copy value , error : ' + err.message);
        }

        document.body.removeChild(textArea);
    }
});