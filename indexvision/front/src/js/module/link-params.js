const linkParams = () => {
    const params = window
        .location
        .search
        .replace('?', '')
        .split('=');

    let result = params[1] - 1;

    if (!isNaN(result)) {
        return result;
    } else {
        return false;
    }
}
export default linkParams();
