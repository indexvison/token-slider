<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php indexvision_schema_type(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php //wp_body_open(); ?>

    <header id="header" class="main-header" role="banner" style="opacity: 0; transform: translateY(-100%);">
        <div class="wrapper">
            <div class="header-visible row">
                <div class="header-icon row">
                    <button class="btn menu-btn">
                        <svg width="32" height="31" viewBox="0 0 32 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="2.90366" cy="2.90366" r="2.90366" fill="white"/>
                            <circle cx="2.9044" cy="15.5" r="2.90366" fill="white"/>
                            <circle cx="2.9044" cy="28.0963" r="2.90366" fill="white"/>
                            <circle cx="15.5021" cy="2.90366" r="2.90366" fill="white"/>
                            <circle cx="15.5021" cy="15.5" r="2.90366" fill="white"/>
                            <circle cx="15.5021" cy="28.0963" r="2.90366" fill="white"/>
                            <circle cx="28.0996" cy="2.90366" r="2.90366" fill="white"/>
                            <circle cx="28.0996" cy="15.5" r="2.90366" fill="white"/>
                            <circle cx="28.0996" cy="28.0963" r="2.90366" fill="white"/>
                        </svg>
                    </button>
                    <a href="" class="telegram-link">
                        <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.0033 0.53125C7.90511 0.53125 0.534546 7.90287 0.534546 17C0.534546 26.0971 7.90617 33.4688 17.0033 33.4688C26.1015 33.4688 33.472 26.0971 33.472 17C33.472 7.90287 26.1004 0.53125 17.0033 0.53125ZM25.0921 11.8139L22.3891 24.5512C22.1894 25.4543 21.6517 25.6732 20.9016 25.2482L16.7844 22.2137L14.7986 24.1262C14.5797 24.3451 14.3938 24.531 13.9688 24.531L14.261 20.3405L21.8908 13.447C22.2234 13.1548 21.8175 12.9891 21.3787 13.2812L11.949 19.2174L7.88492 17.9488C7.00198 17.6704 6.9818 17.0659 8.07086 16.6409L23.9489 10.5177C24.6862 10.2521 25.3301 10.6973 25.091 11.8129L25.0921 11.8139Z" fill="white"/>
                        </svg>
                    </a>
                </div>
                <div class="header-category row">
                    <?php wp_list_categories('orderby=name&style=none&title_li='); ?>
                </div>
            </div>

        </div>

<!--        <nav id="menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">-->
<!--            --><?php //wp_nav_menu(array('theme_location' => 'main-menu', 'link_before' => '<span itemprop="name">', 'link_after' => '</span>')); ?>
<!--        </nav>-->
    </header>