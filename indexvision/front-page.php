<?php
/*
Template Name: Главная
*/
?>

<?php get_header(); ?>

<main class="page-front">

    <div class="wrapper">
        <div class="page-front-title">
            <?php the_content(); ?>
        </div>
    </div>

</main>

<?php get_footer(); ?>
