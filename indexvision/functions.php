<?php

function wpassist_remove_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );

add_action('after_setup_theme', 'indexvision_setup');
function indexvision_setup()
{
    load_theme_textdomain('indexvision', get_template_directory() . '/languages');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('responsive-embeds');
    add_theme_support('automatic-feed-links');
    add_theme_support('html5', array('search-form', 'navigation-widgets'));
    add_theme_support('woocommerce');
    global $content_width;
    if (!isset($content_width)) {
        $content_width = 1920;
    }
    register_nav_menus(array('main-menu' => esc_html__('Main Menu', 'indexvision')));
}

add_action('admin_init', 'indexvision_notice_dismissed');
function indexvision_notice_dismissed()
{
    $user_id = get_current_user_id();
    if (isset($_GET['dismiss']))
        add_user_meta($user_id, 'indexvision_notice_dismissed_7', 'true', true);
}

add_action('wp_enqueue_scripts', 'indexvision_enqueue');
function indexvision_enqueue()
{

    wp_enqueue_style(
        'main-styles',
        get_template_directory_uri() . '/front/dist/css/main.css',
        [],
        '1.0.0',
        'all'
    );
}

add_action('wp_footer', 'indexvision_footer');
function indexvision_footer()
{
    wp_enqueue_script('jquery');

    wp_enqueue_script(
        'slick-js',
        get_template_directory_uri() . '/front/static/vendor/slick/slick.min.js',
        ['jquery'],
        '1.0',
        true
    );

    wp_enqueue_script(
        'main-js',
        get_template_directory_uri() . '/front/dist/bundle.js',
        ['jquery', 'slick-js'],
        '1.0',
        true
    );
}

add_filter('document_title_separator', 'indexvision_document_title_separator');
function indexvision_document_title_separator($sep)
{
    $sep = esc_html('|');
    return $sep;
}

add_filter('the_title', 'indexvision_title');
function indexvision_title($title)
{
    if ($title == '') {
        return esc_html('...');
    } else {
        return wp_kses_post($title);
    }
}

function indexvision_schema_type()
{
    $schema = 'https://schema.org/';
    if (is_single()) {
        $type = "Article";
    } elseif (is_author()) {
        $type = 'ProfilePage';
    } elseif (is_search()) {
        $type = 'SearchResultsPage';
    } else {
        $type = 'WebPage';
    }
    echo 'itemscope itemtype="' . esc_url($schema) . esc_attr($type) . '"';
}

add_filter('nav_menu_link_attributes', 'indexvision_schema_url', 10);
function indexvision_schema_url($atts)
{
    $atts['itemprop'] = 'url';
    return $atts;
}

if (!function_exists('indexvision_wp_body_open')) {
    function indexvision_wp_body_open()
    {
        do_action('wp_body_open');
    }
}
add_action('wp_body_open', 'indexvision_skip_link', 5);
function indexvision_skip_link()
{
    echo '<a href="#content" class="skip-link screen-reader-text">' . esc_html__('Skip to the content', 'indexvision') . '</a>';
}

add_filter('the_content_more_link', 'indexvision_read_more_link');
function indexvision_read_more_link()
{
    if (!is_admin()) {
        return ' <a href="' . esc_url(get_permalink()) . '" class="more-link">' . sprintf(__('...%s', 'indexvision'), '<span class="screen-reader-text">  ' . esc_html(get_the_title()) . '</span>') . '</a>';
    }
}

add_filter('excerpt_more', 'indexvision_excerpt_read_more_link');
function indexvision_excerpt_read_more_link($more)
{
    if (!is_admin()) {
        global $post;
        return ' <a href="' . esc_url(get_permalink($post->ID)) . '" class="more-link">' . sprintf(__('...%s', 'indexvision'), '<span class="screen-reader-text">  ' . esc_html(get_the_title()) . '</span>') . '</a>';
    }
}

add_filter('big_image_size_threshold', '__return_false');
add_filter('intermediate_image_sizes_advanced', 'indexvision_image_insert_override');
function indexvision_image_insert_override($sizes)
{
    unset($sizes['medium_large']);
    unset($sizes['1536x1536']);
    unset($sizes['2048x2048']);
    return $sizes;
}

add_action('widgets_init', 'indexvision_widgets_init');
function indexvision_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar Widget Area', 'indexvision'),
        'id' => 'primary-widget-area',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('wp_head', 'indexvision_pingback_header');
function indexvision_pingback_header()
{
    if (is_singular() && pings_open()) {
        printf('<link rel="pingback" href="%s" />' . "\n", esc_url(get_bloginfo('pingback_url')));
    }
}

add_action('comment_form_before', 'indexvision_enqueue_comment_reply_script');
function indexvision_enqueue_comment_reply_script()
{
    if (get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

function indexvision_custom_pings($comment)
{
    ?>
    <li <?php comment_class(); ?>
            id="li-comment-<?php comment_ID(); ?>"><?php echo esc_url(comment_author_link()); ?></li>
    <?php
}

add_filter('get_comments_number', 'indexvision_comment_count', 0);
function indexvision_comment_count($count)
{
    if (!is_admin()) {
        global $id;
        $get_comments = get_comments('status=approve&post_id=' . $id);
        $comments_by_type = separate_comments($get_comments);
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

// assets
function index_images($name)
{
    echo get_template_directory_uri() . '/front/static/img/' . $name;
}

function index_video($name)
{
    echo get_template_directory_uri() . '/front/static/video/' . $name;
}

// acf option
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Опции',
        'menu_title'	=> 'Опции',
        'menu_slug' 	=> 'acf_option',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}